
This module adds a "minutes" datatype which can be attached
to any event (as defined by event.module).  Minutes can only
be created with reference to a particular event.

REQUIREMENTS
************

This module requires the Event module (http://drupal.org/project/event) to be
installed and enabled.


INSTALLATION
************

1. Copy the 'minutes' directory in the 'modules' directory of your Drupal
   installation.

2. Go to "administer > modules" and enable the Minutes module.

3. Go to "administer > access control" and enable the Minutes permissions for
   the appropriate roles.

4. Go to "administer > settings > content type", and for each content type for
   which you want users to be able to attach minutes, check the "Allow event to
   have minutes" option. Note that this option appears only if the content type
   has first been made "eventable" by selecting either "All views" or "Only in
   views for this type" in the "Show in event calendar" option (this option is
   added by the Event module).
